package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

//Collection defined collection links and name
type Collection struct {
	Name string
	URL  string
}

//Item defined item struct
type Item struct {
	Title                  string `json:"Title"`
	ProdIDAndTag           string `json:"ProdIDAndTag"`
	GtmID                  string `json:"GtmID"`
	GtmPrice               string `json:"GtmPrice"`
	GtmPriceRRP            string `json:"GtmPriceRRP"`
	GtmDiscount            string `json:"GtmDiscount"`
	GtmDiscountValue       string `json:"GtmDiscountValue"`
	GtmPosition            string `json:"GtmPosition"`
	GtmName                string `json:"GtmName"`
	GtmBrandID             string `json:"GtmBrandID"`
	GtmBrandName           string `json:"GtmBrandName"`
	GtmClassification      string `json:"GtmClassification"`
	GtmSubClassification   string `json:"GtmSubClassification"`
	GtmSubClassificationID string `json:"GtmSubClassificationID"`
	GtmBuyingCategory      string `json:"GtmBuyingCategory"`
	GtmSelectedTagID       string `json:"GtmSelectedTagID"`
	GtmProductID           string `json:"GtmProductID"`
	GtmSKU                 string `json:"GtmSKU"`
	GtmPriceEur            string `json:"GtmPriceEur"`
	GtmStatus              string `json:"GtmStatus"`
	GtmHasVideo            string `json:"GtmHasVideo"`
}

var items []Item
var links []Collection

func main() {
	// if len(os.Args) == 1 {
	// 	fmt.Println("Add meg az url ...")
	// 	return
	// }
	// url := os.Args[1]
	// parsingCollection()
	parsingCollectionCategoriesLink()

}

func parsingCollectionCategoriesLink() {
	// var links []Collection

	c := colly.NewCollector(
		colly.AllowedDomains("www.fashiondays.hu"),
		colly.CacheDir("./fashiondays_cache"),
	)

	c.OnHTML("a.main-menu__item-child", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		url := e.Request.AbsoluteURL(link)
		name := e.Text
		//
		collection := Collection{
			Name: name,
			URL:  url,
		}
		links = append(links, collection)

	})
	c.OnScraped(func(r *colly.Response) {
		chooseCategory(links)
	})

	c.Visit("https://www.fashiondays.hu")
}

func chooseCategory(links []Collection) {
	for idx, collection := range links {
		out := fmt.Sprintf("%d: %s", idx, collection.Name)
		fmt.Println(out)
	}
	fmt.Println("Válasz ki melyik termékeket mentsem vagy addj meg egy urlt:")
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	fmt.Println(input.Text())
	param := input.Text()
	idx, err := strconv.ParseInt(param, 10, 32)
	if err != nil {
		//
		// parsingCollection(param)
		collection := Collection{
			Name: "custom_url",
			URL:  param,
		}
		parsingCollection(collection)

	} else {
		linksLength := int64(len(links))
		if idx >= linksLength-1 {
			fmt.Println("Nincs ilyen kategoria")
		}
		collection := links[idx]
		// fmt.Println(collection.URL)
		parsingCollection(collection)

	}

}

func parsingCollection(collection Collection) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.fashiondays.hu"),
		colly.CacheDir("./fashiondays_cache"),
	)
	//

	c.OnHTML("#products-listing", func(e *colly.HTMLElement) {
		var nextPageLink string = ""
		e.DOM.ParentsUntil("~").Find("link").Each(func(_ int, s *goquery.Selection) {
			if property, _ := s.Attr("rel"); strings.EqualFold(property, "next") {
				nextPageLink, _ = s.Attr("href")
			}
		})
		e.ForEach("a.campaign-item", func(_ int, el *colly.HTMLElement) {
			item := Item{
				Title:                  el.Attr("title"),
				ProdIDAndTag:           el.Attr("data-prodaidandtag"),
				GtmID:                  el.Attr("data-gtm-id"),
				GtmPrice:               el.Attr("data-gtm-price"),
				GtmPriceRRP:            el.Attr("data-gtm-price-rrp"),
				GtmDiscount:            el.Attr("data-gtm-discount"),
				GtmDiscountValue:       el.Attr("data-gtm-discount-value"),
				GtmPosition:            el.Attr("data-gtm-position"),
				GtmName:                el.Attr("data-gtm-name"),
				GtmBrandID:             el.Attr("data-gtm-brand-id"),
				GtmBrandName:           el.Attr("data-gtm-brand-name"),
				GtmClassification:      el.Attr("data-gtm-classification"),
				GtmSubClassification:   el.Attr("data-gtm-subclassifaication"),
				GtmSubClassificationID: el.Attr("data-gtm-subclassifaication-id"),
				GtmBuyingCategory:      el.Attr("data-gtm-buying-category"),
				GtmSelectedTagID:       el.Attr("data-gtm-selected-tag-id"),
				GtmProductID:           el.Attr("data-gtm-product-id"),
				GtmSKU:                 el.Attr("data-gtm-sku"),
				GtmPriceEur:            el.Attr("data-gtm-price-eur"),
				GtmStatus:              el.Attr("data-gtm-status"),
				GtmHasVideo:            el.Attr("data-gtm-has-video"),
			}
			items = append(items, item)
		})
		if len(nextPageLink) > 0 {
			c.Visit(nextPageLink)
		} else {
			writeCSV(items, collection.Name)
		}
	})
	//
	// c.OnHTML("a.main-menu__item-child", func(e *colly.HTMLElement) {
	// 	link := e.Attr("href")
	// 	c.Visit(e.Request.AbsoluteURL(link))
	// })
	//
	c.Limit(&colly.LimitRule{
		DomainGlob:  "*",
		RandomDelay: 1 * time.Second,
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})
	c.OnScraped(func(r *colly.Response) {
		fmt.Println("Done")
	})
	c.Visit(collection.URL)
}

func writeCSV(collections []Item, collectionName string) (err error) {
	fileName := GetFilenameDate(collectionName)

	file, err := os.Create(fileName)
	if err != nil {
		return
	}
	defer file.Close()
	//
	w := csv.NewWriter(file)

	err = w.Write([]string{"Title",
		"ProdIDAndTag",
		"GtmID",
		"GtmPrice",
		"GtmPriceRRP",
		"GtmDiscount",
		"GtmDiscountValue",
		"GtmPosition",
		"GtmName",
		"GtmBrandID",
		"GtmBrandName",
		"GtmClassification",
		"GtmSubClassification",
		"GtmSubClassificationID",
		"GtmBuyingCategory",
		"GtmSelectedTagID",
		"GtmSKU",
		"GtmPriceEur",
		"GtmStatus",
		"GtmHasVideo"})
	if err != nil {
		return
	}
	for _, item := range collections {
		err := w.Write([]string{item.Title,
			item.ProdIDAndTag,
			item.GtmID,
			item.GtmPrice,
			item.GtmPriceRRP,
			item.GtmDiscount,
			item.GtmDiscountValue,
			item.GtmPosition,
			item.GtmName,
			item.GtmClassification,
			item.GtmSubClassificationID,
			item.GtmBuyingCategory,
			item.GtmSelectedTagID,
			item.GtmSKU,
			item.GtmPriceEur,
			item.GtmStatus,
			item.GtmHasVideo})
		if err != nil {
			return err
		}
	}
	w.Flush()
	return w.Error()
}

func GetFilenameDate(filename string) string {
	// Use layout string for time format.
	const layout = "2006-01-02_15-04-05"
	t := time.Now()
	return filename + "-" + t.Format(layout) + ".csv"
}
